locales['es'] = {
	"page-title": "Verificador de síntomas de COVID-19",
	"main-title": "¿Es coronavirus?",
	"main-subtitle": "Coronavirus vs Gripe vs. Resfriado común vs Alergias",
	"table-title": "Seleccione síntomas para resaltar enfermedades relacionadas",
	"disclaimer-header": "<strong>ADVERTENCIA:</strong> ¡ESTA NO ES UNA HERRAMIENTA DE DIAGNÓSTICO MÉDICO. ¡CONSULTE SIEMPRE A SU MÉDICO SI TIENE ALGÚN SÍNTOMA!",
	"disclaimer-body": "Esta visualización está destinada a ayudarle a comprender la avalancha de información sobre posibles síntomas de coronavirus. Muestra la probabilidad de que los síntomas sean causados por una de las siguientes cuatro condiciones. Sus síntomas pueden ser causados por una condición diferente no incluida en la herramienta.",
	"disclaimer-agree": "Comprendo y estoy de acuerdo",
	"symptom": "Síntoma",
	"diseases": {
		"covid-19": "COVID-19",
		"common cold": "Resfriado común",
		"flu": "Gripe",
		"allergies": "Alergias"
	},
	"symptoms": {
		"fever": "Fiebre",
		"shortness of breath": "Falta de aire",
		"itchy or watery eyes": "Ojos con picor o llorosos",
		"dry cough": "Tos seca",
		"headaches": "Dolores de cabeza",
		"aches and pains": "Dolores",
		"sore throat": "Dolor de garganta",
		"fatigue": "Fatiga",
		"diarrhea": "Diarrea",
		"runny or stuffy nose": "Nariz congestionada<br/> o con mocos",
		"sneezing": "Estornudos",
		"vomiting": "Vómitos",
		"worsening symptoms": "Empeoramiento<br/> de los síntomas.",
		"history of travel": "Ha viajado recientemente",
		"exposure to known covid-19 patient": "Exposición a un<br/> paciente con COVID-19"
	},
	"frequencies": {
		"common": "Común",
		"rare": "Raro",
		"no": "No",
		"sometimes": "En ocasiones",
		"mild": "Templado",
	},
	"cta": [
		"¿Está interesado en una versión personalizada para su región con cobertura de los laboratorios de tests locales?",
		"¿Está buscando soluciones de telemedicina para la crisis del COVID-19 y la continuidad de la atención para los supervivientes con daño residual de pulmones, corazón y otros órganos?",
		"¿Tiene comentarios e ideas, incluidos datos reales para mejorar el modelo de datos?"
	],
	"cta-button": "Contáctenos",
	"tech-limits-title": "<strong>Tecnología y limitaciones</strong>",
	"tech-limits-text": "Actualmente no tenemos acceso a un corpus de datos lo suficientemente grande como para construir un modelo realista para una evaluación precisa de la probabilidad de enfermedad a partir de los síntomas En cambio, utilizamos una aproximación aproximada basada en la inferencia bayesiana, suponiendo la independencia de los síntomas. La probabilidad de síntomas de la enfermedad se basa en los datos recopilados de diferentes publicaciones por las organizaciones CDC, NIH, OMS, Asthma and Allergy Foundation of America y otras fuentes.",
	"tech-limits-team": "Nuestro equipo tiene muchos años de experiencia en aprendizaje automático, inteligencia artificial y triaje médico y en 2011 introdujo el concepto de <a href='https://en.wikipedia.org/wiki/Computer-aided_simple_triage' target='_blank'>triaje simple asistido por ordenador (CAST)</a> mientras desarrollaba soluciones de imágenes médicas dentro de nuestra compañía hermana <a href='http://rcadia.com/' target='_blank'>Rcadia</a>.",
	"about-us": "Sobre nosotros",
	"terms-of-use": "Términos de uso",
	"privacy-policy": "Política de privacidad",
	"copyrights": "Propiedad intelectual",
	"contact-us": "Contáctenos"
};